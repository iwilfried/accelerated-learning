// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
import { InjectionToken } from '@angular/core';

export const TOGGLE_MENU = new InjectionToken('TOGGLE_MENUdd');
export const environment = {
  production: false,
  firebase: {
    
    apiKey: "AIzaSyAIdZ8iLbenzj7z1AFcj3LDxhBGaCIweyk",
    authDomain: "fruitbase-admin-ionic.firebaseapp.com",
    databaseURL: "https://fruitbase-admin-ionic.firebaseio.com",
    projectId: "fruitbase-admin-ionic",
    storageBucket: "fruitbase-admin-ionic.appspot.com",
    messagingSenderId: "1016143056482",
    appId: "1:1016143056482:web:f4f9d8e70ab93046144b3b"

     // Live
    //  apiKey: "AIzaSyDde9hLB9UiEwblrxWJYCVKiZZs1lrW2ok",
    //   authDomain: "accelerated-learning.firebaseapp.com",
    //   databaseURL: "https://accelerated-learning.firebaseio.com",
    //   projectId: "accelerated-learning",
    //   storageBucket: "accelerated-learning.appspot.com",
    //   messagingSenderId: "627212312296",
    //   appId: "1:627212312296:web:9f40da6677e518d222b35d",
    //   measurementId: "G-46ZPQCFESD"
  },
    ApplicationId:'DwemMbtCTcifvz9LL4T4'
};


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

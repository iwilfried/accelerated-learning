import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children:[
        { path: 'Home', loadChildren: '../home/home.module#homePageModule' },
        { path: 'Syllabus', loadChildren: '../syllabus/syllabus.module#SyllabusPageModule' },
        { path: 'Tests', loadChildren: '../tests/tests.module#TestsPageModule' },
    ]
  },
  {
    path:'',
    redirectTo:'/tabs/Home',
    pathMatch:'full'
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [TabsPage]
})
export class TabsPageModule {}

import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TermslistPage } from './termslist.page';

describe('TermslistPage', () => {
  let component: TermslistPage;
  let fixture: ComponentFixture<TermslistPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TermslistPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TermslistPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

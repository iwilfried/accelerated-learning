import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router, NavigationExtras} from '@angular/router'; 
import { FlashCardService } from 'src/app/shared/fapi/flashcards.service'
import { PageDataService } from 'src/app/shared/pageservice/page-data.service';
import { Location } from '@angular/common';
@Component({
  selector: 'app-termslist',
  templateUrl: './termslist.page.html',
  styleUrls: ['./termslist.page.scss'],
  encapsulation:ViewEncapsulation.None
})
export class TermslistPage implements OnInit {
  public TopicData:any
  public flascardList =[]
  public courseinformation = 'card'
  public chapterNo :number = 0;
  public pointList :any;
  constructor(
    public router:Router,
    public flashservice:FlashCardService,
    private pageService: PageDataService,
    private location: Location,
  ) {
    if(this.router.getCurrentNavigation().extras.state){
      
      this.pointList = this.router.getCurrentNavigation().extras.state.list;
      this.chapterNo = this.pointList.IndexNo
      
      this.TopicData = this.pointList[this.chapterNo]
      this.getFlashcardList(this.TopicData.docId)
      console.log(this.TopicData.docId);
    }

   }
   refreshData(index){
    this.chapterNo = index + 1;
    this.TopicData = this.pointList[this.chapterNo]
    this.getFlashcardList(this.TopicData.docId)
   }


  onBackButtonPress() {
    this.location.back();
  }

  ngOnInit() {
    
  }
  goToFlashcards(){
    if(this.flascardList.length > 0){
      let navigationExtras: NavigationExtras = {
        state: {
          flascardList: this.flascardList,
        }
      };
      this.router.navigate(['questionner'], navigationExtras);
    }
  }
  getFlashcardList(id){
    if(!!id){
      const list: Array<any> = [this.flashservice.getContentWiseData('topicId',id)];
      this.pageService.getList(list).then(r => {
        this.flascardList = r[0]
      });
    }
  }

}

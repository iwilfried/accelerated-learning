import { Component, OnInit,ViewEncapsulation } from '@angular/core';
import { environment } from 'src/environments/environment';
import { SubjectService } from 'src/app/shared/fapi/subject.service'
import { PageDataService } from 'src/app/shared/pageservice/page-data.service';

@Component({
  selector: 'app-syllabus',
  templateUrl: './syllabus.page.html',
  styleUrls: ['./syllabus.page.scss'],
  encapsulation:ViewEncapsulation.None
})
export class SyllabusPage implements OnInit {
  ErrorText: string;
  syllabus: string;

  constructor(
    public SubjectService:SubjectService,
    private pageService: PageDataService,
  ) { }

  ngOnInit() {
    this.getSubjectList()
  }
  async getSubjectList(){
    console.log("getSubjectList")
    if(!!environment.ApplicationId){
      const list: Array<any> = [this.SubjectService.getContentWiseData('application_id',environment.ApplicationId)];
      this.pageService.getList(list).then(r => {
        
        if(r[0].length > 0){
          this.syllabus = r[0][0].courseDetails
        }else{
          console.log("empty data")
        }
        
      }).catch((err)=>{
        console.log(err)
        this.ErrorText = "Subject not found"
      });
    }
  }

}

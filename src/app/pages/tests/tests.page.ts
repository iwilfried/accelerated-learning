import { Component, OnInit,ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-tests',
  templateUrl: './tests.page.html',
  styleUrls: ['./tests.page.scss'],
  encapsulation:ViewEncapsulation.None
})
export class TestsPage implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}

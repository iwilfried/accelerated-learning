import { Component, OnInit,ViewEncapsulation } from '@angular/core';
import { ApplicationService } from 'src/app/shared/fapi/application.service';
import { environment } from 'src/environments/environment';
import { PageDataService } from 'src/app/shared/pageservice/page-data.service';
import { Location } from '@angular/common';
@Component({
  selector: 'app-contact',
  templateUrl: './contact.page.html',
  styleUrls: ['./contact.page.scss'],
  encapsulation:ViewEncapsulation.None
})
export class ContactPage implements OnInit {
  ErrorText = "Contact not found"
  contactString:string
  constructor(
    public ApplicationService:ApplicationService,
    private pageService: PageDataService,
    private location: Location,
  ) { }

  ngOnInit() {
    if(!!environment.ApplicationId){
      const list: Array<any> = [this.ApplicationService.getContentWiseData('docId',environment.ApplicationId)];
      this.pageService.getList(list).then(r => {
        
        if(r[0].length > 0){
          this.contactString = r[0][0].contact
        }else{
          console.log("empty data")
        }
        
      }).catch((err)=>{
        console.log(err)
      });
    }
  }
  goBack(){
    this.location.back()
  }

}

import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ApplicationService } from 'src/app/shared/fapi/application.service';
import { environment } from 'src/environments/environment';
import { PageDataService } from 'src/app/shared/pageservice/page-data.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-imprint',
  templateUrl: './imprint.page.html',
  styleUrls: ['./imprint.page.scss'],
  encapsulation:ViewEncapsulation.None
})
export class ImprintPage implements OnInit {
  ErrorText = "Imprit not found"
  imprintString:string
  constructor(
    public ApplicationService:ApplicationService,
    private pageService: PageDataService,
    private location: Location,
  ) { }

  ngOnInit() {

    if(!!environment.ApplicationId){
      const list: Array<any> = [this.ApplicationService.getContentWiseData('docId',environment.ApplicationId)];
      this.pageService.getList(list).then(r => {
        console.log(r[0])
        if(r[0].length > 0){
          // console.log(r[0][0].about)
          this.imprintString = r[0][0].imprint
        }else{
          console.log("empty data")
        }
        
      }).catch((err)=>{
        console.log(err)
      });
    }
  }
  goBack(){
    this.location.back()
  }

}

import { Component, OnInit,ViewEncapsulation } from '@angular/core';
import { ApplicationService } from 'src/app/shared/fapi/application.service';
import { environment } from 'src/environments/environment';
import { PageDataService } from 'src/app/shared/pageservice/page-data.service';
import { Location } from '@angular/common';
@Component({
  selector: 'app-about',
  templateUrl: './about.page.html',
  styleUrls: ['./about.page.scss'],
  encapsulation:ViewEncapsulation.None
})
export class AboutPage implements OnInit {
  ErrorText = "About not found"
  AboutString:string

  constructor(
    public ApplicationService:ApplicationService,
    private pageService: PageDataService,
    private location: Location,
  ) { }

  ngOnInit() {

    if(!!environment.ApplicationId){
      const list: Array<any> = [this.ApplicationService.getContentWiseData('docId',environment.ApplicationId)];
      this.pageService.getList(list).then(r => {
        
        if(r[0].length > 0){
          // console.log(r[0][0].about)
          this.AboutString = r[0][0].about
        }else{
          console.log("empty data")
        }
        
      }).catch((err)=>{
        console.log(err)
        this.ErrorText = "About not found"
      });
    }
  }
  goBack(){
    this.location.back()
  }



}

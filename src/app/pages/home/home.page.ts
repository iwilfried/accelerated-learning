import { Component, OnInit, ViewEncapsulation, APP_ID } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router'; 
import { ApplicationService } from 'src/app/shared/fapi/application.service';
import { PageDataService } from 'src/app/shared/pageservice/page-data.service';
import { SubjectService } from 'src/app/shared/fapi/subject.service'
import { TopicService } from 'src/app/shared/fapi/topic.service'
import { FlashCardService } from 'src/app/shared/fapi/flashcards.service'
import { AngularFirestore } from '@angular/fire/firestore';
import { TopicModel } from 'src/app/shared/model/topic.model';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
  encapsulation:ViewEncapsulation.None
})

export class homePage implements OnInit {

    courseinformation:string= "overview";
    public subjectList :any;
    
    
    public Subjectname:any;
    public overview:any;
    public Syllabus:any;

    public flascardList = []
    public sendData = [];
    public items = []
    public ErrorText :any;
    
    constructor(
      public router:Router,
      private pageService: PageDataService,
      public ApplicationService:ApplicationService,
      public SubjectService:SubjectService,
      public TopicService:TopicService,
      public db: AngularFirestore,
      public flashservice:FlashCardService
    ) { }
  
  ngOnInit() {
    console.log("ngOnInit")
    this.getSubjectList();
  }
  onClickAccounting(){
    console.log("onClickAccounting")
    this.router.navigateByUrl('termslist');
  }

  async getSubjectList(){
    console.log("getSubjectList")
    if(!!environment.ApplicationId){
      const list: Array<any> = [this.SubjectService.getContentWiseData('application_id',environment.ApplicationId)];
      this.pageService.getList(list).then(r => {
        
        if(r[0].length > 0){
          this.subjectList = r[0]
          this.Subjectname = this.subjectList[0].subject_name;
          let SubjectId = this.subjectList[0].docId; 
          this.overview = this.subjectList[0].overview;
          this.Syllabus = this.subjectList[0].courseDetails
          this.getTopicsOfSubjects(SubjectId)
        }else{
          console.log("empty data")
        }
        
      }).catch((err)=>{
        console.log(err)
        this.ErrorText = "Subject not found"
      });
    }
  }
  getTopicsOfSubjects(id){
    console.log("getTopicsOfSubjects")
    if(!!id){
        let ChapterList = this.db.collection(this.TopicService.path, ref => ref.where('subject_id', '==', id)).snapshotChanges().pipe(
            map(actions => actions.map(a => {
              const data = a.payload.doc.data() as TopicModel;
              const docId = a.payload.doc.id;
      
              return { docId, ...data };
            }))
          );
           ChapterList.subscribe(r => {
            this.items = r
            console.log(this.items)
            this.setData()
          })
      }
      
    }

    setData(){
      console.log("setData")
      let arr = []
      for (let index = 0; index <  this.items.length; index++) {
        this.items[index];
        if(!this.items[index].parents){
          if(!this.items[index]['child']){
            this.items[index]['child'] = []
          }
          let v = this.getNestedChildren(this.items[index].docId)
          this.items[index]['child'] = v
          arr.push(this.items[index])
        }
      }
      this.sendData = arr
    }
    getNestedChildren(parent) {
      console.log("getNestedChildren")
      var out = []
      for(var i in this.items) {
          if(this.items[i].parents == parent) {
              var children = this.getNestedChildren(this.items[i].docId)
  
              if(children.length) {
                  this.items[i].children = children
              }
              out.push(this.items[i])

          }
      }
      return out
  }
  getFlashcardList(){
    let flashId: any = this.sendData[0].docId
      const list: Array<any> = [this.flashservice.getContentWiseData('topicId',flashId)];
      this.pageService.getList(list).then(r => {
        debugger
        this.flascardList = r[0]
        // console.log(this.flascardList)
        if(this.flascardList.length > 0){
          let navigationExtras: NavigationExtras = {
            state: {
              flascardList: this.flascardList,
            }
          };
          console.log(this.flascardList)
          this.router.navigate(['questionner'], navigationExtras);
        }
      });
  }
}

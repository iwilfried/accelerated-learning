import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { homePage } from './home.page';
import { AccordiolnlistComponent } from 'src/app/shared/components/accordiolnlist/accordiolnlist.component';

const routes: Routes = [
  {
    path: '',
    component: homePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [homePage,AccordiolnlistComponent]
})
export class homePageModule {}

import { Component, OnInit, ViewEncapsulation, ViewChild,Output,EventEmitter } from '@angular/core';
import { Events, IonSlides } from '@ionic/angular';
import { Router } from '@angular/router';
import { AnimationProvider } from '../../shared/providers';
import { FlashCardComponent } from 'src/app/shared/components/flash-card/flash-card.component';
import { Location } from '@angular/common';
@Component({
  selector: 'app-questionner',
  templateUrl: './questionner.page.html',
  styleUrls: ['./questionner.page.scss'],
  encapsulation: ViewEncapsulation.None
})

export class QuestionnerPage implements OnInit {
  @ViewChild('slides', { static: false }) ionSlides: IonSlides;
  @ViewChild(FlashCardComponent, {static: false})
  private flashCardComponent: FlashCardComponent;
  visiblePrev:boolean=false
  visibleNext:boolean=true
  suceessData:any=0;
  lastOptation:string;
  currentFlashResult = [];
  public currentCard:any = 1;
  rejected:any=0;

  
  isLogged = false;
  slideOpts = {
    initialSlide: 0,
    speed: 400,

    breakpoints: {
      320: {
        spaceBetween: 113,
        slidesPerView: 1.5,
      },
      480: {

        spaceBetween: 130,
        slidesPerView: 1.5
      },
      // when window width is >= 640px
      640: {

        spaceBetween: 150,
        slidesPerView: 1.5
      }
    },
    pager: false,

    centeredSlides: true,
  };
  public flashCardList:any
  constructor(
    private router: Router,
    private events: Events,
    private aProvider: AnimationProvider,
    private location: Location,
    ) {
      if(this.router.getCurrentNavigation().extras.state){
        this.flashCardList=this.router.getCurrentNavigation().extras.state.flascardList;
      }
  }

  sideChange(event){
    console.log(event)
  }
  RefreshBtn(){
    this.flashCardComponent.flip()
  }
  ngOnInit() {

    this.events.subscribe('login-success', () => {
      this.isLogged = true;
    });
    this.events.subscribe('logout', () => this.isLogged = false);

  }
  
  onClickset(){
    this.location.back()
  }
  
  calculate(data){
    let existing = this.currentFlashResult.filter(t=>t.cardId == this.currentCard).length;
    if(existing){
      this.currentFlashResult.forEach(item=>{
        if(item.cardId === this.currentCard){
          debugger
          item.result = data
        }
      })
    }else{
      let v=  {cardId:this.currentCard,result:data}
      this.currentFlashResult.push(v)
    }
    this.suceessData = this.currentFlashResult.filter(t=>t.result == true).length;
    this.rejected = this.currentFlashResult.filter(t=>t.result == false).length;
  }

  
 


  async addCart(event, id: number) {
    event.stopPropagation();
  }

  onLogin() {
    this.router.navigateByUrl('/login');
  }



  goCart() {
    this.router.navigateByUrl('/pages/cart');
  }
  previousCard() {
    this.ionSlides.slidePrev();
  }
  nextCard() {
    this.ionSlides.slideNext();
  }

  slideNext(){
    this.visiblePrev=true;
    if(this.currentCard < this.flashCardList.length){
      this.currentCard = this.currentCard + 1
    }
    if(this.currentCard == this.flashCardList.length){
      this.visibleNext = false
    }else{
      this.visibleNext = true
    }
    
  }
  slidePrev(){
    if(this.currentCard > 1){
      this.currentCard = this.currentCard -1
    }
    if(this.currentCard == 1){
     this.visiblePrev = false; 
    }else{
      this.visiblePrev = true; 
    }
  }
  animation(i) {
    return this.aProvider.flipInRight(i);
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { QuestionnerPage } from './questionner.page';
import { FlashCardComponent } from 'src/app/shared/components/flash-card/flash-card.component';

const routes: Routes = [
  {
    path: '',
    component: QuestionnerPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [QuestionnerPage,FlashCardComponent]
})
export class QuestionnerPageModule { }

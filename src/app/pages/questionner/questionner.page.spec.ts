import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionnerPage } from './questionner.page';

describe('QuestionnerPage', () => {
  let component: QuestionnerPage;
  let fixture: ComponentFixture<QuestionnerPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuestionnerPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionnerPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

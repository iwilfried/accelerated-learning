import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { MyError } from './myerror';
import { BaseService } from './base.service';
import { SubjectModel } from '../model/subject.model';

@Injectable({
  providedIn: 'root'
})
export class SubjectService extends BaseService<SubjectModel>{

  constructor(public db: AngularFirestore,
    public myErr: MyError) {
      super(db, myErr, 'Subject')
     }
}

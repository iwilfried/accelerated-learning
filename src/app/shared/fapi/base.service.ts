import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { MyError } from './myerror';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

export class BaseService<T> {
    public baseCollection: AngularFirestoreCollection<T>;

    constructor(public db: AngularFirestore,
        public myErr: MyError,
        public path: string) {
        this.baseCollection = this.db.collection<T>(path);
    }

    getList(): Observable<Array<T>> {
        return this.baseCollection.snapshotChanges()
            .pipe(
                map((d) => {
                    return d.map((action) => {
                        const data = action.payload.doc.data();
                        return ({ docId: action.payload.doc.id, ...data }) as T;
                    });
                }),
                catchError(this.myErr.handleError)
            );
    }

    getModel(id: any): Observable<T> {
        return this.baseCollection.snapshotChanges()
            .pipe(
                map((d) => {
                    return d.map((action) => {
                        const data = action.payload.doc.data();
                        return ({ docId: action.payload.doc.id, ...data }) as T;
                    }).find(s => s['id'] === id);
                }),
                catchError(this.myErr.handleError)
            );
    }

    addModel(item: T) {
        return this.baseCollection.add(JSON.parse(JSON.stringify(item)));
    }

    create(c: new (s) => T, id: string): T {
        return new c(id);
    }
    getContentWiseData(searchBy: string,searchText: string): Observable<T[]> {
        const collection = this.db.collection<T>(`/${this.path}`, ref => ref.where(searchBy, '==', searchText))
        return collection.snapshotChanges()
            .pipe(
                map((d) => {
                    return d.map((action) => {
                        const data = action.payload.doc.data();
                        return ({ docId: action.payload.doc.id, ...data }) as T;
                    });
                }),
                catchError(this.myErr.handleError)
            );
    }
}

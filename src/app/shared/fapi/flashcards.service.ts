import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { MyError } from './myerror';
import { BaseService } from './base.service';
import { FlashCardModel } from '../model/flashcard.model';


@Injectable({ providedIn: 'root' })
export class FlashCardService extends BaseService<FlashCardModel> {
  constructor(public db: AngularFirestore,
    public myErr: MyError) {
    super(db, myErr, 'Flashcard');
  }
}



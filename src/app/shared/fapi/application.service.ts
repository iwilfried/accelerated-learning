import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { MyError } from './myerror';
import { BaseService } from './base.service';
import { ApplicationModel } from '../model/application.model';

@Injectable({
  providedIn: 'root'
})
export class ApplicationService extends BaseService<ApplicationModel>  {
  constructor(public db: AngularFirestore,
    public myErr: MyError) {
    super(db, myErr, 'Application');
  }
}

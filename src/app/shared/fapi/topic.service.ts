import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { MyError } from './myerror';
import { BaseService } from './base.service';
import { TopicModel } from '../model/topic.model';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TopicService extends BaseService<TopicModel>{

  constructor(public db: AngularFirestore,
    public myErr: MyError) {
      super(db, myErr, 'Topic')
     }
}
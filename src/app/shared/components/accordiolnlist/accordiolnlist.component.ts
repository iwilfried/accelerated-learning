import { Component, OnInit,ViewEncapsulation, Input,} from '@angular/core';
import { Router, NavigationExtras } from '@angular/router'; 
@Component({
  selector: 'app-accordiolnlist',
  templateUrl: './accordiolnlist.component.html',
  styleUrls: ['./accordiolnlist.component.scss'],
  encapsulation:ViewEncapsulation.None
})
export class AccordiolnlistComponent implements OnInit {
  @Input() sendData :any;
  showDescription:boolean=true
  showitem:boolean=false
  showtitle:any
  showtopicdescription:boolean=true
  expandAll = true;
  
 
constructor(
  public router:Router,
) { }

    ngOnInit() {
      console.log("ngOnInit")
    }
    collapseAllFun(){
      console.log("collapseAllFun")
      this.expandAll = false
      this.showDescription=!this.showDescription
    }
    expandAllFun(){
      console.log("expandAllFun")
        this.expandAll = true
        this.showDescription=!this.showDescription
    }
    toggleAccordian(chaptertitle){
      console.log("toggleAccordian")
      if(chaptertitle.child && chaptertitle.child.length > 0){
        this.expandAll = false
        this.showtitle=chaptertitle.topic_title
        this.showDescription=!this.showDescription
      }else{
        this.GoToTopicPage([chaptertitle],0)
      }
     
    }
    toggleTopicDescription(data){
      console.log("toggleTopicDescription")
      this.showitem = data
      this.showtopicdescription=!this.showtopicdescription
    }    
    GoToTopicPage(list,index){
      console.log("GoToTopicPage")
      list['IndexNo'] = index
      let navigationExtras: NavigationExtras = {
        state: {
          list: list,
        }
      };
      this.router.navigate(['termslist'], navigationExtras);
    }
}   
  


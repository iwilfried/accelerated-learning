import { Component, OnInit, ViewEncapsulation , Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'flash-card',
  templateUrl: './flash-card.component.html',
  styleUrls: ['./flash-card.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FlashCardComponent implements OnInit {
  @Output() AnswerData = new EventEmitter<any>();

  flipped: boolean = false;
  goit:number=0;
  missit:number=0;
  
  constructor() { }

  ngOnInit() { }
  flip() {
    this.flipped = !this.flipped;

  }
  answerBtn(data){
    this.AnswerData.emit(data);
  }
 
 

}

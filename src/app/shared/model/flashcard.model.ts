
export class FlashCardModel {
  public docId?: string;
  public front: string;
  public back: string;
  public topicId:any;
  constructor(flashCard: any = {}) {
    this.front = flashCard.subCateId;
    this.docId = flashCard.docId;
    this.back = flashCard.img;
    this.topicId = flashCard.topicId
  }
}

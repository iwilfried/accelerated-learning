export class SubjectModel {
    public docId?: string;
    public application_id:string;
    public subject_name:string;
    public courseDetails:string;
    public overview:string;
    constructor(model:any={}) {
        this.docId=model.docId;
        this.application_id=model.application_id;
        this.subject_name = model.subject_name;
        this.courseDetails = model.courseDetails;
        this.overview = model.overview
    }
}

export class TopicModel {
    public docId?: string;
    public subject_id:string;
    public topic_title:string;
    public topic_description:string;
    public craditScore:string;
    public parents:Array<any>;
    constructor(model:any={}) {
        this.docId=model.docId;
        this.subject_id=model.subject_id;
        this.topic_title = model.topic_title;
        this.topic_description = model.topic_description;
        this.parents = model.parents
    }
}

export class ApplicationModel {
    public docId?: string;
    public applicationName: string;
    constructor(model:any){
        this.docId = model.id 
        this.applicationName = model.applicationName
    }
}

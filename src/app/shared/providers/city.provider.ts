import {Injectable} from '@angular/core';

@Injectable({providedIn: 'root'})
export class CityProvider {
    public district = [{
        value: '340000',
        label: 'US States',
        children: [{
            value: '341500',
            label: 'New York',
            children: [{
                value: '341522',
                label: 'New York',
                children: []
            }, {
                value: '341525',
                label: 'Buffalo',
                children: []
            }, {
                value: '341502',
                label: 'Rochester',
                children: []
            }, {
                value: '341503',
                label: 'Yonkers',
                children: []
            }, {
                value: '341504',
                label: 'Syracuse',
                children: []
            }, {
                value: '341504',
                label: 'Albany',
                children: []
            }, {
                value: '341504',
                label: 'New Rochelle',
                children: []
            }, {
                value: '341504',
                label: 'Mount Vernon',
                children: []
            }]
        }, {
            value: '110100',
            label: 'California',
            children: [{
                value: '110114',
                label: 'Los Angeles',
                children: []
            }, {
                value: '110105',
                label: 'San Diego',
                children: []
            }, {
                value: '110103',
                label: 'San Jose',
                children: []
            }]
        },
            {
                value: '110100',
                label: 'Arizona',
                children: [{
                    value: '110114',
                    label: 'Phoenix',
                    children: []
                }, {
                    value: '110105',
                    label: 'Tucson',
                    children: []
                }, {
                    value: '110103',
                    label: 'Mesa',
                    children: []
                }]
            }]
    }];

}
